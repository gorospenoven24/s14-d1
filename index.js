// syntax and statement
console.log("Hello Batch 138");

let myVariable;

console.log(myVariable);
// console.log(hello);
let productName = 'desktop computer'
console.log(productName);

let productPrice = 18999;
console.log(productPrice)


/*Reassigning variable value*/
productName = 'Laptop'
console.log(productName);

const interest = 3.539
console.log(interest);

let	supplier;

supplier = "John Smith Tradings";
console.log(supplier);



// Declaring multiple variables

let	productCode = "DC017"
const productBrand = "Dell"

console.log(productCode, productBrand);


// Data types--------------------

let	 country = 'Phlippines';
let	province = "Metro manila"
let	fullAddress = province + ", " + country
console.log(fullAddress);


console.log("Welcome to " + fullAddress + "!");

// escape character-----------------------
let	mailAddress ="Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John's employee went home early";
console.log(message);

message = 'john\'s employee went home early';
console.log(message)


// Numeric data types-------------------------
// Integers / whole numbers

let	headcount = 26;
console.log(headcount);

// Decimal numbers data type--------------------
let	grade = 98.7;
console.log(grade);

// exponential notation ---------------------
let	planetDistance = 2e10;
console.log(planetDistance);


// combining text and strings------------------
console.log("John's grade last quarter is " + grade)


// Boolean datatype---------------------
// 1 / 0 or True/False

let isMarried = true;
let	isGoodConduct = false;
console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Array
// Array is aspecial kind of data type that is ised to store mutlitple valuse
// arrays can store diff. data type but is normally use to storesmilar data types.


let	grades = [98.7, 92.1, 90.2 , 94.6];
console.log(grades);

/* Objects
	---- another special kindof data type tahts used to mimic real world object/items

	sytanx:
		let/const objectName ={
			propertyA: value,
			propertyB: value
		}

*/

let person = {
	fullName: "Noven Gorospe",
	age: 25,
	isMarried: false,
	contact: ["0909090909","2323232323"],
	address: {
		houseNumber: 1234,
		city: "Marikina"

	}
}
console.log(person);


// Null and undefiend
/*Null 
	used to intentionaly express the absence of a value in a variable declaration

*/

let	spouse = null;
let money =0;
let myName = "";

/*Undefined
	represents the state of a variable that has been declared but without an assigned value

*/

/*

functions
	-Functions is js are lines/blocks of codes that tell our device/ applicaion for perform a certain task when called/invoked

	syntax:
	function functionName(){
		line/block of codes;
	}

*/

// declaring function
/*function printName() {
	console.log("My name is noven");
}

// invoking/calling a function
printName();*/


// declare function without invoke

function animal(){
	console.log("My favorite animal is Lion");
}
animal();

// (name) - parameter
// parameter - acts as a name of variable/container taht exixts only inside of a function
function printName(name){
	console.log("My name is " + name);
}

// argument - actual value that is provided a function for it to work properly
printName("Noven");
printName("jane");

function argumentFunction(){
	console.log("this function was passed as a argument before the message was printed");
}
 function invokeFunction(argumentFunction){
 	argumentFunction();
 }
 invokeFunction(argumentFunction);

// finding more informatio about a function in the console
 console.log(argumentFunction);


 // Using multiple parameters

 function createFullname(firstName, middleName, lastName){
 	console.log("Hello " + firstName + " " + middleName + " " + lastName);
 }
 createFullname("Noven","Daquioag", "Gorospe");
 createFullname("Noven", "Daquioag");
 createFullname("Noven","Daquioag", "Gorospe", " Junior");

// using variables as arguments

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullname(firstName, middleName, lastName);


// the return statement
// return allow the output of a function to be passed to the line/bock of that invoked/called

function returnFullname(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName;
	console.log("a simple message");
}

let completeNme = returnFullname(firstName, middleName, lastName);
console.log(completeNme);



